### Satélites argentinos SAC-D / Aquarius, SAC-C, SAC-A, SAC-B y SAOCOM, aspectos técnicos, propósito e instrumental científico.

Realizar un trabajo de investigación conjunto en se que se haga un abordaje completo de los siguientes temas:

* Breve descripción de los principales organismos involucrados en la construcción de satélites argentinos.
* Línea de tiempo con los principales hitos en la carrera satelital argentina
* Descripción técnica, instrumental y misiones del los satélites SAC-D / Aquarius, SAC-C, SAC-A, SAC-B y SAOCOM.
* Vida media del programa satelital correspondiente a cada satélite y estado actual. 
* Breve descripción de los aparatos de medición montados, para qué sirven y alguna curva o foto con los resultados obtenidos
* Discusión de la importancia de los datos provistos por los satélites para la economía y desarrollo del país.
* Citar la bibliografía

Se sugiere la siguiente bibliografía para comenzar

(invap)[http://saocom.invap.com.ar/]

(eoportal: SAC-D / Aquarius)[https://directory.eoportal.org/web/eoportal/satellite-missions/s/sac-d]

(eoportal: SAC-C)[https://directory.eoportal.org/web/eoportal/satellite-missions/s/sac-c]

(eoportal: SAOCOM)[https://directory.eoportal.org/web/eoportal/satellite-missions/s/saocom]

(argentina.gob.ar)[https://www.argentina.gob.ar/ciencia/conae/misiones-espaciales/misiones-cumplidas]

(De Córdoba al espacio: cómo se fabrica y controla un satélite argentino)[https://www.youtube.com/watch?v=H8uwSkZ6gM4&feature=emb_title]

(#SAOCOM1A #AltoEnElCielo - El documental)[https://www.youtube.com/watch?v=S__M_HzNupk]

Lorena Drewes. El sector espacial argentino Instituciones referentes proveedores y desaíios-ARSAT (2014)
